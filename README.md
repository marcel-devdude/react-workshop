# Yarn

[See Installation instruction](https://yarnpkg.com/en/docs/install)

## MacOS

```
brew install yarn
```

# CRA

```
npx create-react-app react-workshop --typescript
cd react-workshop
rm -rf .git
yarn start
```

## Eject

```
yarn eject
yarn add @babel/plugin-transform-react-jsx-source @babel/plugin-transform-react-jsx-self date-fns serve webpack-bundle-analyzer
```


# ESLint & Prettier

```
yarn add eslint-plugin-prettier eslint-config-prettier prettier
```


# Storybook

```
yarn add @types/storybook__react @types/storybook__addon-actions @storybook/addon-knobs @types/storybook__addon-knobs
npx -p @storybook/cli sb init --type react
```

# Styled-Components

```
yarn add styled-components @types/styled-components polished
```

# React Spring

```
yarn add react-spring
```

# React Router

```
yarn add react-router-dom @types/react-router-dom
```

# Redux

```
yarn add redux react-redux @types/redux @types/react-redux
```

# MobX

```
yarn add mobx mobx-react
```

# Formik

```
yarn add formik yup @types/yup
```

# Axios

```
yarn add axios @types/axios
```

# Apollo

```
yarn add apollo-client apollo-cache-inmemory apollo-link-http react-apollo graphql-tag graphql
```

# Enzyme

```
yarn add enzyme enzyme-adapter-react-16 react-test-renderer @types/enzyme
```

# Storyshots

```
yarn add @storybook/addon-storyshots @storybook/addon-storyshots-puppeteer babel-plugin-require-context-hook react-test-renderer
```

```
"scripts": {
  ...
  "storyshots": "jest --config storyshots/jest.config.js"
  ...
},
"babel": {
  "presets": [
    "react-app"
  ],
  "env": {
    "test": {
      "plugins": [
        "require-context-hook"
      ]
    }
  }
},
```

# Cypress

```
yarn add cypress
```

# Links

- [Collection of React Hooks](https://nikgraf.github.io/react-hooks/)
- [Crooks](https://github.com/chrisjpatty/crooks)
- [Awesome React Hooks](https://github.com/rehooks/awesome-react-hooks)
- [Downshift](https://github.com/downshift-js/downshift)
- [Reach UI](https://ui.reach.tech/)
- [Webpack offline plugin](https://github.com/NekR/offline-plugin)
- [apollo-link-batch-http](https://www.npmjs.com/package/apollo-link-batch-http)
- [babel-plugin-styled-components](https://github.com/styled-components/babel-plugin-styled-components)
- [babel-plugin-polished](https://github.com/styled-components/babel-plugin-polished)
- [immer](https://github.com/immerjs/immer)
- [React DnD](https://github.com/react-dnd/react-dnd)
- [Helmet](https://github.com/nfl/react-helmet)
- [Popper](https://github.com/FezVrasta/react-popper)
- [Webpack Bundle Analyzer](https://www.npmjs.com/package/webpack-bundle-analyzer)

