import React from 'react';
import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { GlobalStyles } from '../src/components/GlobalStyles'
import { theme } from '../src/styles/theme';
import { ThemeProvider } from 'styled-components';

// automatically import all files ending in *.stories.js
const req = require.context('../src/components', true, /\.stories\.tsx$/);

const GlobalStyleDecorator = ({ children }) => (
  <>
    {children}
    <GlobalStyles />
  </>
);

const GlobalThemeDecorator = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
);

function loadStories() {
  addDecorator(story => <GlobalThemeDecorator>{story()}</GlobalThemeDecorator>);
  addDecorator(story => <GlobalStyleDecorator>{story()}</GlobalStyleDecorator>);
  addDecorator(withKnobs);

  req.keys().forEach(filename => req(filename));
  // require('../src/components/Overlay/index.stories')
}

configure(loadStories, module);
