import initStoryshots from '@storybook/addon-storyshots';
import { imageSnapshot } from '@storybook/addon-storyshots-puppeteer';

const getMatchOptions = () => ({
  failureThreshold: 0,
  failureThresholdType: 'percent',
});

const test = imageSnapshot({
  storybookUrl: 'http://localhost:6006',
  getMatchOptions,
  customizePage: page => page.setViewport({ width: 1280, height: 720 }),
});

initStoryshots({
  suite: 'Storyshots',
  // storyNameRegex: /^(?!overview).*$/, // Ignore "overview"
  test,
});
