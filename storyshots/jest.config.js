module.exports = {
  testMatch: [`**/storyshots/storyshots.ts`],
  /* transform: {
    '^.+\\.tsx?$': 'babel-jest',
  }, */
  setupFiles: ['<rootDir>/setup.js'],
  "transform": {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/../node_modules/babel-jest",
    "^.+\\.css$": "<rootDir>/../config/jest/cssTransform.js",
    "^(?!.*\\.(js|jsx|ts|tsx|css|json)$)": "<rootDir>/../config/jest/fileTransform.js"
  },
  "transformIgnorePatterns": [
    "[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$",
    "^.+\\.module\\.(css|sass|scss)$"
  ],
};
